#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <sys/utsname.h>


MODULE = Package    PACKAGE = Package   PREFIX=bind_

SV *
bind_uname()
    CODE:
        struct utsname buffer;
        uname(&buffer);
        RETVAL = newSVpv(buffer.sysname, 0);
    OUTPUT:
        RETVAL
