package Package;
use strict;
use warnings;
use version 0.77; our $VERSION = qv("v0.0.1");

require Exporter;
require AutoLoader;
require XSLoader;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(uname);
sub AUTOLOAD { }

XSLoader::load(__PACKAGE__, $VERSION);

1;
__END__
=encoding utf8

=head1 NAME

Package - Dummy package linked to libc

=head1 SYNOPSIS

  use Package;
  Package::uname();

=head1 DESCRIPTION

=over 8

=item B<uname()>

Returns kernel name.

=back

=cut
