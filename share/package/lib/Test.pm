package Test;

# ^perl\(VMS should be filtered from requires
require "VMS" if 0;
require "VMS::A" if 0;

# ^perl\(Win32 should be filtered from requires
require "Win32" if 0;
require "Win32::A" if 0;

# ^perl\(VMS should be filtered from provides
package VMS;
package VMS::A;

# ^perl\(Win32 should be filtered from provides
package Win32;
package Win32::A;

# ^perl\(DB\)$ should be filtered from provides
package DB;

# ^perl\(UNIVERSAL\)$ should be filtered from provides
package UNIVERSAL;

# Package filtered by spec file
package Test::Private
1
