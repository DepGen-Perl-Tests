Name:           package
Version:        0
Release:        1%{?dist}
Summary:        Package to test Perl dependency generator
License:        GPLv3+
URL:            http://example.com/
Source0:        package.tar
BuildRequires:  findutils
BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  perl-devel
BuildRequires:  perl-generators
BuildRequires:  perl(AutoLoader)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(XSLoader)
BuildRequires:  perl(version) >= 0.77
# Tests only:
BuildRequires:  perl(Test::Simple)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(Test::Private\\)
%{?perl_default_filter}

%description
Test package.

%prep
%setup -q -n %{name}

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -delete
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -delete
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc example/
%{perl_vendorarch}/*
%{_mandir}/man3/*

%changelog
* Tue Oct 22 2013 Petr Pisar <ppisar@redhat.com> - 0-1
- Dummy package
